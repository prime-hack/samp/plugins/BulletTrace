// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#include "TracePool.h"

#include "TraceConfig.h"

TracePool::TracePool() {
	if ( TraceConfig::get().GetPoolSize() == kDefaultTraceCount ) return;
	resize( TraceConfig::get().GetPoolSize() );
}

TracePool &TracePool::get() {
	static TracePool self;
	return self;
}

CBulletTrace *TracePool::pool() const {
	if ( pool_.pool ) return pool_.pool;

	if ( *(std::uint8_t *)kTracePoolPointerInstructionAddress == kTracePoolPointerInstructionCode )
		return (CBulletTrace *)( *(std::uintptr_t *)kTracePoolPointerAddress - kTracePoolOffset );
	return (CBulletTrace *)kTracePoolAddress;
}

std::size_t TracePool::size() const {
	if ( pool_.pool ) return pool_.size;

	if ( *(std::uint16_t *)kCMPInstructionAddress != kCMPInstructionCode ) return kDefaultTraceCount;

	auto end_of_pool = *(std::uintptr_t *)( kCMPInstructionAddress + 2 );

	return ( end_of_pool - std::uintptr_t( pool() ) + kTracePoolOffset ) / sizeof( CBulletTrace );
}

CBulletTrace *TracePool::operator[]( std::uint32_t id ) const {
	return (CBulletTrace *)( std::uintptr_t( TracePool::get().pool() ) + id * sizeof( CBulletTrace ) );
}

void TracePool::resize( std::uint32_t size ) {
	if ( size == kDefaultTraceCount ) {
		auto old_pool = pool_.pool;
		pool_.pool = nullptr;
		delete[] old_pool;
	}

	auto new_pool = new CBulletTrace[size];
	auto copy_size = std::min( size, TracePool::size() );
	std::memcpy( new_pool, TracePool::pool(), copy_size );

	auto old_pool = pool_.pool;
	if ( size < TracePool::size() ) pool_.size = size;
	pool_.pool = new_pool;
	if ( size >= TracePool::size() ) pool_.size = size;
	delete[] old_pool;
}
