// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#ifndef TRACE_API_H
#define TRACE_API_H

#ifdef EXPORT_API_SYMBOLS
#	define TRACE_API __declspec( dllexport )
#else
#	define TRACE_API __declspec( dllimport )
#endif


#if __cplusplus
class TraceConfig &TRACE_API GetConfig();
#	define DECL_ENUM enum class
#else
#	define DECL_ENUM enum
#endif
DECL_ENUM eTraceMode{ /// depends by weapon id
					  weapon_depends = 0,
					  /// depends by player color
					  nick_depends,
					  /// depends by victim type
					  victim_depends
};
#undef DECL_ENUM

enum eLongTracers {
	kShortTracers = false,
	kLongTracers
};

enum eOverrideOldTracers {
	kWaitFreeTracers = false,
	kOverridOldTracers
};

const int kVehicleWeaponId = 49;
const int kDefaultWeaponId = 254;
const int kMissWeaponId = kDefaultWeaponId - 1;
const int kObjectWeaponId = kMissWeaponId - 1;

#if __cplusplus
extern "C" {
#endif
eTraceMode TRACE_API GetTraceMode();
void TRACE_API SetTraceMode( eTraceMode mode );

eLongTracers TRACE_API GetLongTracers();
void TRACE_API SetLongTracers( eLongTracers long_tracers );

eOverrideOldTracers TRACE_API GetOverrideOldern();
void TRACE_API SetOverrideOldern( eOverrideOldTracers override_oldern );

int TRACE_API GetPoolSize();
void TRACE_API SetPoolSize( int size );

/// @return ARGB color
unsigned TRACE_API GetWeaponColor( int weapon_id );
void TRACE_API SetWeaponColor( int weapon_id, unsigned colorARGB );

/// @return time in milliseconds
unsigned TRACE_API GetWeaponLifeTime( int weapon_id );
void TRACE_API SetWeaponLifeTime( int weapon_id, unsigned life_time_ms );

float TRACE_API GetWeaponRadius( int weapon_id );
void TRACE_API SetWeaponRadius( int weapon_id, float radius );

void TRACE_API SaveConfig();
void TRACE_API LoadConfig();
#if __cplusplus
}
#endif

#endif // TRACE_API_H
