#ifndef MAIN_H
#define MAIN_H

#include <memory>

#include <SRDescent/SRDescent.h>

#include "loader/loader.h"

class AsiPlugin : public SRDescent {
	std::unique_ptr<class TraceRender> render_;
	std::unique_ptr<class TraceAdd> add_;
	std::unique_ptr<class TraceExtend> extend_;

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();
};

#endif // MAIN_H
