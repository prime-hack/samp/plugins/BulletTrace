// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#include "TraceRender.h"

#include "TracePool.h"
#include "Log.h"

TraceRender::TraceRender() {
	renderPtr_.onAfter += std::tuple{ this, &TraceRender::GetRenderPtr };
	renderPtr_.install( 0, 0, false );
	renderColor_.onBefore += std::tuple{ this, &TraceRender::GetRenderColor };
	renderColor_.install( 0, 0, false );
	renderCount_.onBefore += std::tuple{ this, &TraceRender::GetRenderCount };
	renderCount_.install( 0, 0, true );
}

void TraceRender::GetRenderPtr( SRHook::Info &info ) {
	info.cpu.ESI = std::uintptr_t( &TracePool::get().pool()->m_vecStart.fZ );

	info.retAddr = 0x723CBD;
}

void TraceRender::GetRenderColor( SRHook::CPU &cpu ) {
	auto trace = (CBulletTrace *)( cpu.ESI - 8 );
	cpu.EDI = ( 0x80 << 24 ) | ( trace->color.red << 16 ) | ( trace->color.green << 8 ) | trace->color.blue;
}

void TraceRender::GetRenderCount( SRHook::CPU &cpu ) {
	if ( cpu.ESI < std::uintptr_t( &TracePool::get().pool()->m_vecStart.fZ ) + sizeof( CBulletTrace ) * TracePool::get().size() )
		cpu.SF = !cpu.OF;
	else
		cpu.SF = cpu.OF;
}
