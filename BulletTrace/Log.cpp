// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#include "Log.h"

#include <filesystem>

#include <windows.h>

#include "Utility.h"

std::ostream &Log::details::log_stream() {
	static std::ofstream out;
	if ( out.is_open() ) return out;

	auto module_path = Utility::current_module();

	std::string file_name = "BulletTrace";
	if ( module_path.has_stem() ) file_name = module_path.stem().string();

	if ( module_path.has_parent_path() )
		out.open( module_path.parent_path() / ( file_name + ".log" ) );
	else
		out.open( ( file_name + ".log" ) );

	if ( !out.is_open() ) {
		static bool warn_access = false;
		if ( !warn_access ) {
			warn_access = true;
			MessageBoxA( 0, "Can't write log file", file_name.c_str(), 0 );
		}
	}
	return out;
}
