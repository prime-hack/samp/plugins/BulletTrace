// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#include "TraceAPI.h"

#include "TraceConfig.h"

TraceConfig &GetConfig() {
	return TraceConfig::get();
}

eTraceMode GetTraceMode() {
	return TraceConfig::get().TraceMode();
}

void SetTraceMode( eTraceMode mode ) {
	TraceConfig::get().TraceMode() = mode;
}

eLongTracers GetLongTracers() {
	return TraceConfig::get().LongTracers();
}

void SetLongTracers( eLongTracers long_tracers ) {
	TraceConfig::get().LongTracers() = long_tracers;
}

eOverrideOldTracers GetOverrideOldern() {
	return TraceConfig::get().OverrideOldern();
}

void SetOverrideOldern( eOverrideOldTracers override_oldern ) {
	TraceConfig::get().OverrideOldern() = override_oldern;
}

int GetPoolSize() {
	return TraceConfig::get().GetPoolSize();
}

void SetPoolSize( int size ) {
	TraceConfig::get().SetPoolSize( size );
}

unsigned GetWeaponColor( int weapon_id ) {
	auto color = TraceConfig::get().GetWeaponColor( weapon_id );
	return ( color.blue << 24 ) | ( color.green << 16 ) | ( color.red << 8 ) | 0x80;
}

void SetWeaponColor( int weapon_id, unsigned colorARGB ) {
	auto color = (std::uint8_t *)&colorARGB;
	TraceConfig::get().SetWeaponColor( weapon_id, { color[2], color[1], color[0] } );
}

unsigned GetWeaponLifeTime( int weapon_id ) {
	return TraceConfig::get().GetWeaponLifeTime( weapon_id );
}

void SetWeaponLifeTime( int weapon_id, unsigned life_time_ms ) {
	TraceConfig::get().SetWeaponLifeTime( weapon_id, life_time_ms );
}

float GetWeaponRadius( int radius ) {
	return TraceConfig::get().GetWeaponRadius( radius );
}

void SetWeaponRadius( int weapon_id, float radius ) {
	TraceConfig::get().SetWeaponRadius( weapon_id, radius );
}

void SaveConfig() {
	TraceConfig::get().Save();
}

void LoadConfig() {
	TraceConfig::get().Load();
}
