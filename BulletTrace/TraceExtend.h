// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#ifndef TRACEEXTEND_H
#define TRACEEXTEND_H

#include <llmo/SRHook.hpp>

class TraceExtend {
	SRHook::Hook<> updatePoolPtr_{ 0x723FB0, 8 };
	SRHook::Hook<> updatePoolSize_{ 0x7240AF, 6 };
	SRHook::Hook<> addTracePoolPtr_{ 0x72375A, 6 };
	SRHook::Hook<> addTracePoolSize_{ 0x7237AA, 5 };
	SRHook::Hook<struct RwV3D *, struct RwV3D *, float, unsigned int, char> addTraceInsert_{ 0x723810, 5 };

public:
	TraceExtend();

protected:
	std::uint32_t nextTraceId_ = 0;

	void GetUpdatePoolPtr( SRHook::Info &info );
	void GetUpdatePoolSize( SRHook::CPU &cpu );
	void GetAddTracePoolPtr( SRHook::CPU &cpu );
	void GetAddTracePoolSize( SRHook::Info &info );
	void GetAddTraceInsert( SRHook::Info &info,
							struct RwV3D *&pStartPoint,
							struct RwV3D *&pEndPoint,
							float &radius,
							unsigned int &dissapearTime,
							char &alpha );
};

#endif // TRACEEXTEND_H
