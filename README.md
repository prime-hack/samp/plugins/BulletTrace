 [![pipeline status](https://gitlab.com/prime-hack/samp/plugins/BulletTrace/badges/master/pipeline.svg)](https://gitlab.com/prime-hack/samp/plugins/BulletTrace/-/commits/master)

# BulletTrace

Plugin to modify bullet tracers in **GTA: San Andreas**. Its support:

- Use color, life time and radius depends by weapon
- Use color, life time and radius depends by nametag color (SAMP\*)
- Use color, life time and radius depends by victim type

\* Supported SAMP versions: 0.3.7-R1, 0.3.7-R2, 0.3.7-R3, 0.3.7-R4, 0.3DL

## Configuration

Config file is json. This contain sections with params of tracers.

#### Weapon settings

Each weapon section contain next settings:

```json
      "color": {
        "blue": 0,
        "green": 255,
        "red": 255
      },
      "life_time": 300,
      "radius": 0.009999999776482582
```

- **color** - RGB color of trace
  - **blue** - Blue channel of color [0-255]
  - **green** - Green channel of color [0-255]
  - **red** - Red channel of color [0-255]
- **life_time** - Life time of trace in milliseconds
- **radius** - Radius of trace

#### Global settings

```json
  "mode": 2,
  "long_tracers": 1,
  "override_oldern": 0,
  "pool_size": 16
```

- **mode** - Mode of select trace params
  - **0** - Params depends by weapon
  - **1** - Params depends by nametag color
  - **2** - Params depends by victim type
- **long_tracers** - Use long tracers
  - **0** - Short tracers
  - **1** - Long tracers
- **override_oldern** - Override old tracers
  - **0** - Wait to free space in pool for new tracers
  - **1** - Override old tracers
- **pool_size** - Size of tracers pool (16 is default)
